/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.a70iii.oxgame;

/**
 *
 * @author Surap
 */
import java.util.Scanner;

public class GAME {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);

        boolean stopGame = false;
        System.out.println(!stopGame);
        while (!stopGame) {
            playGame();
            String conState;
            System.out.print("Continue (Y/N) ? : ");
            conState = kb.next();
            if (conState.equals("N")) {

                System.out.println("Thank You For Play Game.");
                System.out.println("Goodbye.");
                stopGame = true;
            }
        }
    }

    public static void playGame() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Welcome to OX Games");
        System.out.println("Table of numbers");
        String[][] table = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
        boolean wins = false;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
        int tik = 1;
        String tok = "O";
        int nub=11;
        while (!wins) {
            if(nub ==9){break;}
            System.out.println("Turn " + tok);
            System.out.print("Please Input Number to Place : ");
            int Selex = kb.nextInt();
            int ii = 0;
            int jj = 0;
            if (Selex == 1) {
                ii = 0;
                jj = 0;
            } else if (Selex == 2) {
                ii = 0;
                jj = 1;
            } else if (Selex == 3) {
                ii = 0;
                jj = 2;
            } else if (Selex == 4) {
                ii = 1;
                jj = 0;
            } else if (Selex == 5) {
                ii = 1;
                jj = 1;
            } else if (Selex == 6) {
                ii = 1;
                jj = 2;
            } else if (Selex == 7) {
                ii = 2;
                jj = 0;
            } else if (Selex == 8) {
                ii = 2;
                jj = 1;
            } else if (Selex == 9) {
                ii = 2;
                jj = 2;
            }else{
                System.out.println("Try Again!!!");
                continue;}
            
            
            nub = tik;
            if (table[ii][jj].equals("X") || table[ii][jj].equals("O")) {
                System.out.println("Try Again!!!");
                nub--;
                continue;
            }
            table[ii][jj] = tok;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(table[i][j] + " ");
                }
                System.out.println();

            }
            if (tik == 10) {
                for (int k = 0; k < 3; k++) {
                    if (table[0][k].equals(tok) && table[1][k].equals(tok) && table[2][k].equals(tok)) {
                        wins = true;
                        break;

                    }

                }
                for (int g = 0; g < 3; g++) {
                    if (table[g][0].equals(tok) && table[g][1].equals(tok) && table[g][2].equals(tok)) {
                        wins = true;
                        break;

                    }

                }
                if (table[0][0].equals(tok) && table[1][1].equals(tok) && table[2][2].equals(tok)) {
                    wins = true;
                    tik--;
                    break;

                }
                if (table[0][2].equals(tok) && table[1][1].equals(tok) && table[2][0].equals(tok)) {
                    wins = true;
                    tik--;
                    break;

                }
                
                break; // มันยังไม่ได้กรณีที่ชนะตอนวาง9ช่องครบ 1 2 3 4 5 7 6 8 9
            }

            for (int k = 0; k < 3; k++) {
                if (table[0][k].equals(tok) && table[1][k].equals(tok) && table[2][k].equals(tok)) {
                    wins = true;
                    tik--;
                    break;

                }

            }
            for (int g = 0; g < 3; g++) {
                if (table[g][0].equals(tok) && table[g][1].equals(tok) && table[g][2].equals(tok)) {
                    wins = true;
                    tik--;
                    break;

                }

            }
            if (table[0][0].equals(tok) && table[1][1].equals(tok) && table[2][2].equals(tok)) {
                wins = true;
                break;

            }
            if (table[0][2].equals(tok) && table[1][1].equals(tok) && table[2][0].equals(tok)) {
                wins = true;
                break;

            }
            tik++;
            if ((tik % 2) == 0) {
                tok = "X";
            } else {
                tok = "O";
            }

        }
        if (tik < 9) {
            System.out.println(tok + " Win!!!");
        } else {
            System.out.println("Draw!!!");
        }

    }
}
